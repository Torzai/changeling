import { OnInit, OnDestroy, Component } from '@angular/core';
import * as AOS from 'aos';

//@AutoUnsubscribe()
@Component({
    selector: 'main-container',
    templateUrl: './main.container.html'
})

export class MainContainerComponent implements OnInit, OnDestroy {
    ngOnInit() {
        AOS.init({
            duration: 800,
            easing: 'slide',
            once: true
        });
    }
    ngOnDestroy() {
    }

}
