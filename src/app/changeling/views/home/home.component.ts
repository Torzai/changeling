import { OnInit, OnDestroy, Component } from '@angular/core';
import { Router } from '@angular/router';


//@AutoUnsubscribe()
@Component({
  selector: 'home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {
  SlideOptions = { items: 1, dots: true, nav: true };  
  CarouselOptions = { items: 3, dots: true, nav: true };  
  title = 'owlcarouselinAngular';  
  Images = ['assets/images/person_1.jpg', 'assets/images/person_1.jpg', 'assets/images/person_3.jpg', 'assets/images/person_2.jpg'];

  public isCollapsed: number = 0;
  public coworkers: any = [
    {
      title: 'Nosolorol',
      description: 'Nosolorol Ediciones es una editorial española especialista en rol que apuesta por la unión entre el formato tradicional y el electrónico. Tiene un amplio catálogo de juegos a un precio inmejorable que podéis consultar en su página web.',
      page: 'https://www.nosolorol.com/es/'
    },
    {
      title: 'RevCC',
      description: 'RevCC es una asociación de rol en vivo similar a la nuestra que nació en Cáceres. Hacen un gran número de vivos de todo tipo con una gran calidad todos ellos. Os recomendamos que echéis un ojo a su página web.',
      page: 'http://revcc.es/'
    },
    {
      title: 'Crónicas Nocturnas',
      description: 'Crónicas Nocturnas es un proyecto nacido de la idea de crear una ambientación nacional y que muchas ciudades con muchos vivos diferentes puedan colaborar entre ellas.',
      page: 'http://www.cronicasnocturnas.es/'
    }
  ];
  public mainTitle = {
    title: 'Changeling',
    subTitle: 'Lágrimas por Calíope'
  }

  public mainActivity = [
    { title: 'LARP', description: 'Partidas de rol en vivo de diferentes ambientaciones.', icon: 'fab fa-superpowers' },
    { title: 'Talleres', description: 'Talleres de introducción a relacionados con el rol en vivo.', icon: 'fas fa-chalkboard-teacher' },
    { title: 'Rol Mesa/Online', description: 'Partidas online y en mesa para complementar los personajes.', icon: 'fas fa-dice-d20' }
  ]

  public news = [
    {
      title: 'Primeras Jornadas de Roleros Sin Rol',
      description: 'La asociación Vilnoc convoca las primeras jornadas de roleros sin rol. ¡Apúntate!',
      image: 'assets/images/rolsinrol.jpeg'
    },
    {
      title: 'Titánic',
      description: 'De como algunas hadas viajaron a bordo del Insumerginle, vieron un mundo forjado de sueños y lucharon contra un coloso de hielo.',
      image: 'assets/images/titanic.jpg'
    },
    {
      title: 'El Décimotercer Vagón',
      description: 'De como tres hadas subieron a un tren con un vagón mágico, resolvieron un misterio y detuvieron una oscura profecía.',
      image: 'assets/images/vagonnum13.jpg'
    }
  ]

  constructor(private router: Router) {
  }

  ngOnInit() {
    
  }
  ngOnDestroy() {
  }

  toggleCollapse(i: number) {
    this.isCollapsed = i;
  }
}
