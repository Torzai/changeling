import { OnInit, OnDestroy, Component } from '@angular/core';

//@AutoUnsubscribe()
@Component({
  selector: 'footer-component',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit, OnDestroy {

  constructor(){
  }

  ngOnInit(){
  }
  ngOnDestroy(){
  }
}
