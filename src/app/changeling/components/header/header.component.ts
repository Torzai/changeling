import { OnInit, OnDestroy, Component } from '@angular/core';
import { Router } from '@angular/router';
import '@fortawesome/fontawesome-free/css/all.min.css';


//@AutoUnsubscribe()
@Component({
  selector: 'header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {

  public activeItem: string = 'home';
  public menuContent: any;

  constructor(private router: Router) {
    this.menuContent = [
      { value: 'home', title: 'Home', hasChildren: [] },
      { value: 'project', title: 'Proyectos', hasChildren: [] },
      { value: 'chronicle', title: 'Crónica', hasChildren: [{ value: 'cronicaSangre', title: 'Crónicas de la Sangre' }, { value: 'lagrimasCaliope', title: 'Lágrimas por Calíope' }] },
      { value: 'stories', title: 'Relatos', hasChildren: [] },
      { value: 'aboutus', title: 'Sobre Nosotros', hasChildren: [] }, 
      { value: 'contact', title: 'Contacto', hasChildren: []}
    ];
  }

  ngOnInit() {
    this.router.navigate(['/', this.activeItem]);
  }
  ngOnDestroy() {
  }

  goToItem(key: string) {
    this.activeItem = key;
    this.router.navigate(['/', key]);
  }

}
