import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainContainerComponent } from './container/main-container/main.container';
import { HomeComponent } from './views/home/home.component';
import { AboutUsComponent } from './views/aboutus/aboutus.component';
import { ProjectComponent } from './views/projects/project.component';
import { ChronicleComponent } from './views/chronicle/chronicle.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: '',
    component: MainContainerComponent,
    children: [
      { path: 'home', component: HomeComponent},
      { path: 'project', component: ProjectComponent},
      { path: 'aboutus', component: AboutUsComponent },
      { path: 'chronicle', component: ChronicleComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class ChangelingRoutingModule { }
