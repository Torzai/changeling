import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangelingRoutingModule } from './changeling-routing.module';

import { MainContainerComponent } from './container/main-container/main.container';

import { HomeComponent } from './views/home/home.component';

import { HomeService } from './views/home/home.service';
import { AboutUsComponent } from './views/aboutus/aboutus.component';
import { AboutUsService } from './views/aboutus/aboutus.service';
import { ProjectComponent } from './views/projects/project.component';
import { OwlModule } from 'ngx-owl-carousel';  
import { ChronicleComponent } from './views/chronicle/chronicle.component';


@NgModule({
  declarations: [
    MainContainerComponent,
    HomeComponent,
    AboutUsComponent,
    ProjectComponent, 
    ChronicleComponent
  ],
  imports: [
    CommonModule,
    ChangelingRoutingModule,
    OwlModule
  ], 
  providers: [
    HomeService, 
    AboutUsService
  ]
})
export class ChangelingModule { }
