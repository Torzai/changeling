import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OwlModule } from 'ngx-owl-carousel';  

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ChangelingModule } from './changeling/changeling.module';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './changeling/components/header/header.component';
import { FooterComponent } from './changeling/components/footer/footer.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';


@NgModule({
  declarations: [
    AppComponent, 
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    BrowserModule, 
    OwlModule,
    ChangelingModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot([])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
